'use strict';
const compression = require('compression');
const express = require('express');
const path = require('path');

const publicRoot = path.join(__dirname, '/public');
const env = process.env;
const app = express();

env.NODE_ENV = env.NODE_ENV || 'development';

// todo: better log and debug
console.log(`The environment is "${env.NODE_ENV}".`);
const isProd = env.NODE_ENV === 'production';

// todo: add port and host config
// long term caching
app.set('port', env.NODE_PORT || '3000');
app.set('address', env.NODE_IP || 'localhost');
app.disable('x-powered-by');

if (!isProd) {
  app.use((req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
  });
}

// todo: For a high-traffic website in production, the best way to put compression in place is to implement it at a reverse proxy level.
app.use(compression());

// IMPORTANT: Some hosting services require to respond to GET /health with status 200 for health monitoring purpose.
app.get('/health', (req, res) => {
  res.status(200).end();
});

// long-term caching
app.get(/^\/assets\/(.*\/)*[^/]+\.v-\w+.\w+$/, express.static(publicRoot, {
  maxAge: '30d',
}));

app.get('/assets/*', express.static(publicRoot));
app.get('*', (req, res) => {
  res.sendFile('index.html', {root: publicRoot});
});

app.use((req, res) => {
  res.status(404).end('y u do dis?');
});

// todo: error handlers

if (app.get('address') === '*') {
  app.listen(app.get('port'), () => {
    console.log(`Server process ${process.pid} started on *:${app.get('port')} ...`);
  });
} else {
  app.listen(app.get('port'), app.get('address'), () => {
    console.log(`Server process ${process.pid} started on ${app.get('address')}:${app.get('port')} ...`);
  });
}
