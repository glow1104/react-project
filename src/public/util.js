'use strict';

export const generateId = (() => {
  const letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  var nextId = 0;

  function idfy(s) {
    return s ? s.toString().replace(/\W/g, '_') : '';
  }

  return mark => letters[nextId % letters.length] + (++nextId) + idfy(mark);
})();

export function nextTick(fn) {
  var cancelled = false;
  Promise.resolve().then(() => {
    if (!cancelled) {
      fn();
    }
  });
  return () => {cancelled = true;};
}

export function nextTask(fn) {
  const timeout = setTimeout(() => {fn();});
  return () => {clearTimeout(timeout);};
}
