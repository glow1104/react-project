'use strict';
import {connect} from 'react-redux';
import React from 'react';

import FormInput from './FormInput';
import {changeFilterOrder} from '../actions';

const MIN_FILTER_ORDER = 1;
const MAX_FILTER_ORDER = 20;

function validate(value) {
  if (!value) {
    return false;
  }
  value = parseInt(value, 10);
  return value >= MIN_FILTER_ORDER && value <= MAX_FILTER_ORDER;
}

function onChange(dispatch, event) {
  const value = event.target.value;
  if (!value || validate(value)) {
    dispatch(changeFilterOrder(value));
  }
}

export default connect(
  state => ({value: state.get('filterOrder')}),
  dispatch => ({onChange: onChange.bind(null, dispatch)})
)(props => <FormInput label="Filter Order" placeholder type="number" charPattern={/[0-9]/} validate={validate} {...props}/>);
