'use strict';
import {Button} from 'semantic-ui-react';
import React from 'react';

export default function({onClick, title, primary, loading, disabled, children, ...props}) {
  disabled = loading || disabled;
  return (
    <Button title={title} primary={primary}
            onClick={disabled ? null : onClick} loading={loading && !disabled} disabled={disabled} {...props}>
      {children}
    </Button>
  );
}
