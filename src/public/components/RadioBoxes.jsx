'use strict';
import {Form} from 'semantic-ui-react';
import React from 'react';

import {generateId} from '../util';
import RadioBox from './RadioBox';

export default class extends React.Component {
  componentWillMount() {
    const radios = this.props.radios || [];
    this._radios = radios.map(({id, ...rest}) => (
      {id: id || generateId(rest.label || rest.name), ...rest}
    ));
  }

  render() {
    const {name, label, value, defaultValue, onChange, inline, grouped, equalWidth, noWrap} = this.props;
    const radioBoxes = this._radios.map(radio => (
      <RadioBox name={name} onChange={onChange}
                checked={value === undefined ? undefined : value === radio.value}
                defaultChecked={defaultValue === undefined ? undefined : defaultValue === radio.value}
                key={radio.id} {...radio}/>
    ));
    return (
      <Form.Field inline={inline}>
        <label>{label}</label>
        <Form.Group inline={inline} style={noWrap ? {flexWrap: 'nowrap'} : undefined} grouped={!inline && grouped} widths={equalWidth ? radioBoxes.length : undefined}>
          {radioBoxes}
        </Form.Group>
      </Form.Field>
    );
  }
}
