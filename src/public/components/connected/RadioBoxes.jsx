'use strict';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {changeInput} from '../../actions';
import RadioBoxes from '../RadioBoxes';

export default connect(
  (state, {name}) => ({value: state.get(name)}),
  (dispatch, {name}) => bindActionCreators({
    onChange: changeInput.bind(null, name),
  }, dispatch)
)(RadioBoxes);
