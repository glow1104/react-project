'use strict';
import React from 'react';
import {Link} from 'react-router';

export default function({children, style, ...props}) {
  return (
    <Link activeClassName="active" className="item"
          style={{outline: 'none', ...style}} {...props}>
          {children}
    </Link>
  );
}
