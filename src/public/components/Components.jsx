'use strict';
import React from 'react';
import {Grid} from 'semantic-ui-react';

export function Column({children}) {
  return (
    <Grid.Column width={12} mobile={6} tablet={4} computer={3} largeScreen={2} widescreen={2}>
    {children}
    </Grid.Column>
  );
}
