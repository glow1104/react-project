'use strict';
import React from 'react';
import {Menu} from 'semantic-ui-react';

import NavLink from './components/NavLink';

const TOP_MENU_HEIGHT = 50;
const SIDE_MENU_WIDTH = 250;

const subNavLinkStyle = {
  lineHeight: 1.3,
  marginTop: 1,
};

export default function({children}) {
  return (
    <div style={{paddingTop: TOP_MENU_HEIGHT, paddingLeft: SIDE_MENU_WIDTH}}>
      <Menu fixed="top" size="large" borderless style={{height: TOP_MENU_HEIGHT, zIndex: 102, border: 'none'}}>
        <Menu.Item header className="h3 inverted" style={{width: SIDE_MENU_WIDTH, justifyContent: 'center'}}>
          MATRISYNTHEX
        </Menu.Item>
      </Menu>
      <Menu fixed="left" size="large" vertical inverted style={{width: SIDE_MENU_WIDTH, marginTop: TOP_MENU_HEIGHT}}>
        <Menu.Item>SYNTHESIS MATRIX
          <Menu.Menu>
            <NavLink to="/coupling-matrix-synthesis" style={subNavLinkStyle}>Coupling Matrix Synthesis</NavLink>
          </Menu.Menu>
        </Menu.Item>
        <NavLink to="/computer-aided-design">COMPUTER AIDED DESIGN</NavLink>
      </Menu>
      {children}
    </div>
  );
}
