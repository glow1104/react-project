'use strict';

/*-------------------
        Page
--------------------*/

export const textColor = 'rgba(0, 0, 0, 0.82)';

/*-------------------
        Grid
--------------------*/

export const columnCount = 12;

/*-------------------
      Site Colors
--------------------*/

/*---  Colors  ---*/
export const blue = '#2185D0';
export const teal = '#00B5AD';
export const grey = '#6a6a6a';
export const black = '#2d2d2d';

/*---  Light Colors  ---*/
export const lightGrey = '#DCDDDE';

/*---   Neutrals  ---*/
export const offWhite = '#FAFAFA';
export const darkWhite = '#F3F4F5';

/*-------------------
     Alpha Colors
--------------------*/

export const subtleTransparentBlack = 'rgba(0, 0, 0, 0.03)';
export const transparentBlack = 'rgba(0, 0, 0, 0.05)';
export const strongTransparentBlack = 'rgba(0, 0, 0, 0.10)';
export const veryStrongTransparentBlack = 'rgba(0, 0, 0, 0.15)';

/*-------------------
     Neutral Text
--------------------*/

export const mutedTextColor = 'rgba(0, 0, 0, 0.6)';

export const unselectedTextColor = 'rgba(0, 0, 0, 0.4)';

/*-------------------
      Borders
--------------------*/

export const solidBorderColor = '#D4D4D5';

/*-------------------
    Brand Colors
--------------------*/

export const primaryColor = blue;
export const blueishBlack = '#2F4D6B';

/*-------------------
    Emotive Colors
--------------------*/
export const disabledBackgroundColor = '#E0E1E2';
