'use strict';
import 'babel-polyfill';
import 'whatwg-fetch';

import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import {Router, Route, IndexRedirect, Redirect, browserHistory} from 'react-router';
import thunk from 'redux-thunk';

import appReducer from './reducers';
import defaultState from './default-state';
import App from './App';
import CplMatSyn from './CplMatSyn';

const store = createStore(appReducer, defaultState, applyMiddleware(thunk));

ReactDOM.render((
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRedirect to="coupling-matrix-synthesis"/>
        <Route path="coupling-matrix-synthesis" component={CplMatSyn}/>
        <Redirect from="*" to="coupling-matrix-synthesis"/>
      </Route>
    </Router>
  </Provider>
), document.getElementById('root'));
