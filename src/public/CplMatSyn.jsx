'use strict';
import {Form, Grid, Segment, Button} from 'semantic-ui-react';
import React from 'react';

import {Column} from './components/Components';
import {Section, FormInput} from './components/ConnectedComponents';
import RadioBoxes from './components/connected/RadioBoxes';
import LoadingButton from './components/LoadingButton';
import TransZerosAccordion from './components/TransZerosAccordion';
import PhysTopoMatEditor from './components/PhysTopoMatEditor';
import PhysTopoGraphViewer from './components/PhysTopoGraphViewer';
import FilterOrderInput from './components/FilterOrderInput';

export default class extends React.Component {
  constructor() {
    super();
    this.state = {validity: {
      parameters: {
        filterOrder: true,
      },
    }};
  }

  _validSection(section) {
    const validity = this.state.validity[section];
    for (const field in validity) {
      if (!validity[field]) {
        return false;
      }
    }
    return true;
  }

  _valid() {
    const validity = this.state.validity;
    for (const section in validity) {
      if (!this._validSection(section)) {
        return false;
      }
    }
    return true;
  }

  _handleValidityChange(section, field, valid) {
    const validity = this.state.validity;
    validity[section][field] = valid;
    this.setState({validity});
  }

  render() {
    return (
      <div>
        <Segment basic>
          <LoadingButton primary disabled={!this._valid()}>
            Calculate
          </LoadingButton>
        </Segment>
        <Section title="Parameters" name="cplMatSyn_parameters" error={!this._validSection('parameters')}>
          <Form>
            <Grid>
              <Column>
                <FilterOrderInput onValidityChange={this._handleValidityChange.bind(this, 'parameters', 'filterOrder')}/>
              </Column>
              <Column>
                <FormInput label="Start Frequency" name="startFreq" placeholder suffix="GHz" type="number" min="0"/>
              </Column>
              <Column>
                <FormInput label="Stop Frequency" name="stopFreq" placeholder suffix="GHz" type="number" min="0"/>
              </Column>
              <Column>
                <FormInput label="Return Loss" name="returnLoss" placeholder suffix="dB" type="number"/>
              </Column>
              <Column>
                <FormInput label="Unloaded Q" name="unloadedQ" placeholder type="number"/>
              </Column>
              <Column>
                <RadioBoxes label="Mode" name="filterMode" equalWidth noWrap radios={[
                  {label: 'TE', value: 'TE'},
                  {label: 'TEM', value: 'TEM'},
                ]}/>
              </Column>
            </Grid>
          </Form>
        </Section>
        <TransZerosAccordion/>
        <Section title="Physical Topology" name="cplMatSyn_physicalTopology">
          <Grid>
            <Grid.Row>
              <Grid.Column style={{display: 'flex', justifyContent: 'center'}}><PhysTopoMatEditor style={{overflowX: 'auto'}}/></Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column><PhysTopoGraphViewer style={{overflowX: 'auto', textAlign: 'center'}}/></Grid.Column>
            </Grid.Row>
          </Grid>
        </Section>
      </div>
    );
  }
}
