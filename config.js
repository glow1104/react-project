'use strict';

const config = {
  deploy: {
    test: {
      //remote: 'origin'
      //branch: 'master'
      url: 'ssh://580c04bc89f5cf0143000163@test-mengl.rhcloud.com/~/git/test.git',
      //userName:
      //sshAgent: false
      publicKey: '~/.ssh/id_rsa.pub',
      privateKey: '~/.ssh/id_rsa',
      //passphrase:
      //password:
    },
  },
  paths: {
    src: 'src',
    build: 'build',
    cache: 'build/cache',
    dist: 'dist',
    deploy: 'deploy',
    ui: 'src/public/ui',
  }
};

const NODE_ENV = process.env.NODE_ENV || 'development';

try {
  const envConfigName = 'config.' + NODE_ENV + '.js';
  const envConfig = require('./' + envConfigName);
  console.log('Load ' + envConfigName);
  Object.assign(config, envConfig);
} catch (ex) {
  if (!(ex instanceof Error) || ex.code !== 'MODULE_NOT_FOUND') {
    throw ex;
  }
}

module.exports = config;
