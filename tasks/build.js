'use strict';
const fs = require('fs');
const path = require('path');
const sh = require('shelljs');

sh.set('-e');

module.exports = function (projDir) {
  const paths = require('./paths')(projDir);
  sh.cd(paths.proj);

  sh.mkdir('-p', paths.build);
  sh.rm('-rf', paths.dist);
  sh.mkdir('-p', paths.dist);
  sh.cp(sh.find(path.join(paths.src, '/*.js')), paths.dist);

  sh.exec(`npm run gulp -- --gulpfile "${path.join(paths.ui, 'semantic/gulpfile.js')}" build`, process.env);
  sh.exec('npm run bundle', process.env);

  const packageJson = require(path.join(paths.proj, 'package.json'));
  delete packageJson.devDependencies;
  delete packageJson.scripts;
  packageJson.scripts = {start: 'node --use_strict start.js'}
  fs.writeFileSync(path.join(paths.dist, 'package.json'), JSON.stringify(packageJson, null, 2) + '\n', 'utf-8');
}

if (!module.parent) {
  module.exports(path.join(__dirname, '..'));
}
