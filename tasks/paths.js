'use strict';
const path = require('path');
const paths = require('../config').paths;

module.exports = projDir => {
  projDir = path.resolve(projDir);
  return {
    proj: projDir,
    src: path.join(projDir, paths.src),
    build: path.join(projDir, paths.build),
    cache: path.join(projDir, paths.cache),
    dist: path.join(projDir, paths.dist),
    deploy: path.join(projDir, paths.deploy),
    ui: path.join(projDir, paths.ui),
  };
};
