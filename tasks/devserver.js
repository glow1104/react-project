'use strict';
const path = require('path');
const WebpackDevServer = require('webpack-dev-server');
const webpack = require('webpack');

const DEVSERVER_PORT = 3000;
const DEVSERVER_ADDRESS = '0.0.0.0';

module.exports = function (projDir) {
  const paths = require('./paths')(projDir);
  const config = require(path.join(paths.proj, "webpack.config.js"));
  const compiler = webpack(config);
  const server = new WebpackDevServer(compiler, {
    contentBase: path.join(paths.dist, 'public'),
    compress: true,
    stats: {colors: true},
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000,
    },
    historyApiFallback: true,
  });
  server.listen(DEVSERVER_PORT, DEVSERVER_ADDRESS);
}

if (!module.parent) {
  module.exports(path.join(__dirname, '..'));
}
