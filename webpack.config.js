'use strict';
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const InlineManifestWebpackPlugin = require('inline-manifest-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const paths = require('./tasks/paths')(__dirname);

const env = process.env;
env.NODE_ENV = env.NODE_ENV || 'development';
const NODE_ENV = env.NODE_ENV;
const isProd = NODE_ENV === 'production';
const DATAURL_LIMIT = 10000; // ~10k
const nodeModulesDir = path.join(paths.proj, 'node_modules') + path.sep;

module.exports = {
  context: path.join(paths.src, 'public'),
  entry: {
    app: [
      isProd ? './ui/ui' : './ui/ui.min',
      './index',
    ],
  },
  output: {
    path: path.join(paths.dist, 'public/assets'),
    publicPath: '/assets/',
    filename: '[name].v-[chunkhash].js',// test if hash is stable isProd ? '[name].v-[chunkhash].js' : '[name].js',
    chunkFilename: '[id].v-[chunkhash].js', // test if hash is stable isProd ? '[id].v-[chunkhash].js' : '[id].js',
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
  },
  module: {
    loaders: [
      {
        test: /\.jsx$/,
        include: path.join(paths.src, 'public'),
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
          plugins: ['syntax-jsx', 'transform-react-jsx', 'transform-react-display-name', 'transform-object-rest-spread'],
          cacheDirectory: paths.cache,
        }
      },
      {
        test: /\.js$/,
        include: path.join(paths.src, 'public'),
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
          plugins: ['transform-object-rest-spread'],
          cacheDirectory: paths.cache,
        }
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('css'),
      },
      {
        test: /\.(ttf|woff|eot|woff2|svg)$/,
        loader: 'file',
        query: {
          name: isProd ? '[name].v-[hash:base32].[ext]' : '[name].[ext]',
        },
      },
      {
        test: /\.png$/,
        loader: 'url',
        query: {
          name: isProd ? '[name].v-[hash:base32].[ext]' : '[name].[ext]',
          limit: DATAURL_LIMIT,
          mimetype: 'image/png',
        },
      },
    ],
  },
  plugins: ([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(NODE_ENV),
      },
    }),
    new ExtractTextPlugin(isProd ? 'ui.v-[contenthash].css' : 'ui.css', {allChunks: true}),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: module => {
        const resource = module.resource;
        if (!resource) {
          return false;
        }
        return resource.startsWith(nodeModulesDir);
      },
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest',
      filename: path.join(path.relative(path.join(paths.dist, 'public/assets'), paths.build), 'manifest.js'),
    }),
  ]).concat(isProd ? [
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.DedupePlugin(),
  ] : []).concat([
    new InlineManifestWebpackPlugin({name: 'webpackManifest'}),
    new HtmlWebpackPlugin({
      title: env.npm_package_name.toUpperCase(),
      template: './index.html.ejs',
      filename: path.join(paths.dist, 'public/index.html'),
      minify: isProd ? {html5: true} : false,
      env,
    }),
  ]),
};
